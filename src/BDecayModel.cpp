#include "BDecayModel.hh"

#include "RandomNumberGeneration.hh"

using namespace Espresso;

// DEFAULT PDFS

// ACCEPTANCE
const double& Espresso::DefAcceptanceCutoff() {
  static double cutoff = 0.1;
  return cutoff;
}

const Function& Espresso::DefAcceptanceFunc() {
  static Function func = [] (double tau) {
    double cutoff = Espresso::DefAcceptanceCutoff();
    if (tau < cutoff)
      return 0.0;
    else
      return 1.0;
  };
  return func;
}

// RESOLUTION
const double& Espresso::DefResolution() {
  static double res = 0.045;
  return res;
}

const DiracDeltaDistribution& Espresso::DefResolutionDist() {
  static DiracDeltaDistribution dist(DefResolution());
  return dist;
}

const ResolutionModel& Espresso::DefResolutionModel() {
  static ResolutionModel model = [] (double tau, double tauerr) {
    auto _dist = std::make_unique<NormalDistribution>(tau,tauerr);
    return _dist;
  };
  return model;
}

BDecayModel::BDecayModel(CalibrationMode _mode)
  : mode(_mode),
    m_asymmetry(0.0),
    m_AcceptanceFunc(Espresso::DefAcceptanceFunc()),
    m_LifetimeDist(nullptr),
    m_ResolutionDist(nullptr),
    m_ResolutionModel(Espresso::DefResolutionModel()),
    m_DeltaM(ModeFrequency.at(_mode)),
    m_DeltaMErr(ModeFrequencyErr.at(_mode)),
    m_Lifetime(ModeLifetime.at(_mode)),
    m_LifetimeErr(ModeLifetimeErr.at(_mode)),
    m_DeltaGamma(ModeDeltaGamma.at(_mode)),
    m_DeltaGammaErr(ModeDeltaGammaErr.at(_mode))
{
  if (Espresso::PrintHardDebugInfo)
    std::cout << "CONSTRUCTING BDECAYMODEL... ";
  SetLifetimeDist(ExponentialDistribution(0.0,m_Lifetime));
  SetResolutionDist(Espresso::DefResolutionDist());
  if (Espresso::PrintHardDebugInfo)
    std::cout << "done" << std::endl;
}

/// Copy constructor
BDecayModel::BDecayModel(const BDecayModel& rhs)
  : mode(rhs.mode),
    m_asymmetry(rhs.m_asymmetry),
    m_AcceptanceFunc(rhs.m_AcceptanceFunc),
    m_LifetimeDist(rhs.m_LifetimeDist->clone()),
    m_ResolutionDist(rhs.m_ResolutionDist->clone()),
    m_ResolutionModel(rhs.m_ResolutionModel),
    m_DeltaM(rhs.m_DeltaM),
    m_DeltaMErr(rhs.m_DeltaMErr),
    m_Lifetime(rhs.m_Lifetime),
    m_LifetimeErr(rhs.m_LifetimeErr),
    m_DeltaGamma(rhs.m_DeltaGamma),
    m_DeltaGammaErr(rhs.m_DeltaGammaErr)
{
  if (Espresso::PrintHardDebugInfo)
    std::cout << "COPYING BDECAYMODEL" << std::endl;
}

/// Move constructor
BDecayModel::BDecayModel(BDecayModel&& rhs)
  : mode(std::move(rhs.mode)),
    m_asymmetry(std::move(rhs.m_asymmetry)),
    m_AcceptanceFunc(std::move(rhs.m_AcceptanceFunc)),
    m_LifetimeDist(std::move(rhs.m_LifetimeDist)),
    m_ResolutionDist(std::move(rhs.m_ResolutionDist)),
    m_ResolutionModel(std::move(rhs.m_ResolutionModel)),
    m_DeltaM(std::move(rhs.m_DeltaM)),
    m_DeltaMErr(std::move(rhs.m_DeltaMErr)),
    m_Lifetime(std::move(rhs.m_Lifetime)),
    m_LifetimeErr(std::move(rhs.m_LifetimeErr)),
    m_DeltaGamma(std::move(rhs.m_DeltaGamma)),
    m_DeltaGammaErr(std::move(rhs.m_DeltaGammaErr))
{
  if (Espresso::PrintHardDebugInfo)
    std::cout << "MOVING BDECAYMODEL" << std::endl;
}

BDecayModel::~BDecayModel() 
{
  if (Espresso::PrintHardDebugInfo)
    std::cout << "DESTROYING BDECAYMODEL" << std::endl;
}

double BDecayModel::SampleDecayTime() const {
  double tau, acc, thresh;
  do {
    tau = m_LifetimeDist->GetRandom();
    acc = m_AcceptanceFunc(tau);
    thresh = RandomNumberGeneration::Get();
  } while (acc < thresh);
  return tau;
}

double BDecayModel::SampleResolution() const {
  return m_ResolutionDist->GetRandom();
}

const ResolutionModel& BDecayModel::GetResolutionModel() const {
  return m_ResolutionModel;
}

const double& BDecayModel::GetDeltaM() const {
  return m_DeltaM;
}

const double& BDecayModel::GetDeltaMErr() const {
  return m_DeltaM;
}

const double& BDecayModel::GetLifetime() const {
  return m_Lifetime;  
}

const double& BDecayModel::GetLifetimeErr() const {
  return m_LifetimeErr;  
}

const double& BDecayModel::GetDeltaGamma() const {
  return m_DeltaGamma;
}

const double& BDecayModel::GetDeltaGammaErr() const {
  return m_DeltaGammaErr;
}
