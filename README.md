## Compilation using LCG views

If you're using LbEnv (and hence `$CMTCONFIG`) is defined, run
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_97/$CMTCONFIG/setup.sh
```

Then create a build directory and compile the project via
```
mkdir build && cd build
cmake ..
make -j4
```
